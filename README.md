# esa-snap

This image contains [ESA's SNAP](http://step.esa.int/main/toolboxes/snap/) including all the S1TBX, S2TBX, S3TBX, SMOS Box and PROBA-V Toolboxes. It includes the Graph Processing Tool (GPT) for creating user-defined processing chains via the command line.

# Starting an interactive bash session

To start the container and bootstrap a bash session, mapping a host folder as `/data` use:

`docker run -it -v {host/mapped/folder}:/data atavares/esa-snap /bin/bash`


# Graph Processing Tool (GPT)

This image is prepared to execute SNAP raster data operations in batch-mode. Operators can be used standalone or by using a processing graph defined in XML. The [XML graph can be parametrised](https://sa.catapult.org.uk/wp-content/uploads/2016/05/SNAP-Sentinel-1_TrainingCourse_Exercise4.pdf) to pass variables to a Docker run command as shown below. 

## gpt from a bash session

Assuming that `BatchGraph.xml ` has been parametrised as shown in the tutorial link above, one can call `gpt` within an interactive bash session by running:

`gpt {location/of/BatchGraph.xml} -Pinput1="{INPUT11}" -Pinput2="{INPUT12}" -Pinput3="{INPUT13}" -Ptarget1="{TARGET1}"`

## gpt from a host machine / outside the container

One can also call `gpt` outside the container by invoking `Docker run` as below:

`docker run -it -v {host/mapped/folder}:/data atavares/esa-snap gpt {location/of/BatchGraph.xml} -Pinput1="{INPUT11}" -Pinput2="{INPUT12}" -Pinput3="{INPUT13}" -Ptarget1="{TARGET1}" -f {outputFormat}`

See here an example:

`docker run -it -v /home/ubuntu:/data atavares/esa-snap gpt /data/snap-archive/Sentinel-1/Tornado/CCD.xml -Pinput1="/data/snap-archive/Sentinel-1/get_data/SENTINEL/S1A_IW_SLC__1SDV_20180507T225057_20180507T225124_021805_025A2D_3D59.SAFE" -Pinput2="/data/snap-archive/Sentinel-1/get_data/SENTINEL/S1A_IW_SLC__1SDV_20180519T225057_20180519T225124_021980_025FC0_2D54.SAFE" -Ptarget1="/data/snap-archive/Sentinel-1/Tornado/output.tif" -f GeoTIFF-BigTIFF`


## Change the gpt configuration settings 

Sometimes larger graphs result in limited memory caused errors. One way to solve this is by [increasing the java heap size in a configuration file](https://rus-copernicus.eu/forum/t/snap-increasing-of-java-heap-size-in-configuration-files/112). 

One can create/edit a `gpt.vmoptions` file on the host machine with the following contents:

```
# Enter one VM parameter per line
# For example, to adjust the maximum memory usage to 512 MB, uncomment the following line:
# -Xmx512m
# To include another file, uncomment the following line:
# -include-options [path to other .vmoption file]
# my settings:
-Xmx8G
```

In the example above we specify 8 GB as the maximum memory size to be used by the container.

To invoke `gpt` while passing the configuraion settings defined in `gpt.vmoptions` host file one can run:

`docker run -it -v {host/mapped/folder}:/data atavares/esa-snap /bin/bash -c "cp {location/of/gpt.vmoptions} /usr/local/snap/bin/ && gpt {location/of/BatchGraph.xml} -Pinput1="{INPUT11}" -Pinput2="{INPUT12}" -Pinput3="{INPUT13}" -Ptarget1="{TARGET1}" -f {outputFormat}"` 



# Credits

Based on Docker's [mrmoor/esa-snap](https://github.com/schwankner/docker-esa-snap) image.

